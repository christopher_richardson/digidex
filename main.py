from api import get_by_stage, get_by_name, get_by_type

print('welcome to digidex')
print("search by: name, type, stage")

filter  = input("what do you want to search: ")

if filter == 'name':
    digimon  = input("what digimond do you want to search:")
    get_by_name(digimon)

if filter == 'type':
    digimon  = input("what digimond type do you want to search:")
    get_by_type(digimon)

if filter == 'stage':
    digimon  = input("what digimond stage do you want to search:")
    get_by_stage(digimon)
