from typing import Union
import csv

file = 'digidata.csv'
digidex = {}


def get_by_name(nombre):
        with open(file, mode = 'r') as f:
            for digi in csv.DictReader(f, fieldnames=('name','Stage','Type','Attribute','Memory','Equip Slots','Lv 50 HP','Lv50 SP','Lv50 Atk','Lv50 Def','Lv50 Int','Lv50 Spd')):
                digimons = digi
                if digi['name'] == nombre:
                    print(f"{digimons}")
        return get_by_name()

def get_by_type(type):
        with open(file, mode = 'r') as f:
            for digi in csv.DictReader(f, fieldnames=('name','Stage','Type','Attribute','Memory','Equip Slots','Lv 50 HP','Lv50 SP','Lv50 Atk','Lv50 Def','Lv50 Int','Lv50 Spd')):
                digimons = digi
                if digi['Type'] == type:
                    print(f"{digimons}")
        return get_by_type

def get_by_stage(stage):
    with open(file, mode = 'r') as f:
        for digi in csv.DictReader(f, fieldnames=('name','Stage','Type','Attribute','Memory','Equip Slots','Lv 50 HP','Lv50 SP','Lv50 Atk','Lv50 Def','Lv50 Int','Lv50 Spd')):
            digimons = digi
            if digi['Stage'] == stage:
                print(f"{digimons}")
    return get_by_stage